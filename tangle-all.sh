#!/bin/bash
cd ~/dotfiles
for n in `git ls-files *-tangle.org`
do
    emacs --batch --eval "(require 'org)" --eval "(org-babel-tangle-file \"$n\")"
done
